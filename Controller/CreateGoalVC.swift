//
//  CreateGoalVC.swift
//  GOALpost
//
//  Created by Marc Michel on 10/9/18.
//  Copyright © 2018 Marc Michel. All rights reserved.
//

import UIKit

class CreateGoalVC: UIViewController, UITextViewDelegate {

    @IBOutlet weak var textField: UITextView!
    @IBOutlet weak var shortTermBtn: UIButton!
    @IBOutlet weak var longTermBtn: UIButton!
    @IBOutlet weak var nextBtn: UIButton!
    
    var goalType : Terms = .shortTerm
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textField.delegate = self
        nextBtn.bindtoKeyboard()
        shortTermBtn.setSelectedcolor()
        longTermBtn.setDeselectedColor()

    }
    
    @IBAction func nextBtnPressed(_ sender: Any) {
        if textField.text != "" && textField.text != "What is your goal?" {
            guard let finishGoalVC = storyboard?.instantiateViewController(withIdentifier: CREATE_GOAL_VC) as? FinishGoalVC else {return}
            finishGoalVC.initData(description: textField.text!, type: goalType)
            presentDetail(finishGoalVC)
        }
    }
    
    @IBAction func shortTermPressed(_ sender: Any) {
        goalType = .shortTerm
        shortTermBtn.setSelectedcolor()
        longTermBtn.setDeselectedColor()
    }
    
    @IBAction func longTermBtnPressed(_ sender: Any) {
        goalType = .longTerm
        longTermBtn.setSelectedcolor()
        shortTermBtn.setDeselectedColor()
    }
    
    @IBAction func backBtn(_ sender: Any) {
        dismissDetail()
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textField.text = ""
        textField.textColor = #colorLiteral(red: 0.05882352963, green: 0.180392161, blue: 0.2470588237, alpha: 1)
    }
    
}
