//
//  FinishGoalVCViewController.swift
//  GOALpost
//
//  Created by Marc Michel on 10/11/18.
//  Copyright © 2018 Marc Michel. All rights reserved.
//

import UIKit
import CoreData

class FinishGoalVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var createGoalBtn: UIButton!
    
    var goalDescription: String!
    var goalType: Terms!
    
    func initData(description: String, type: Terms) {
        self.goalType = type
        self.goalDescription = description
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        textField.delegate = self
        createGoalBtn.bindtoKeyboard()

    }
    
    @IBAction func createGoalBtnPressed(_ sender: Any) {
        if textField.text != nil {
            save { (complete) in
                if complete {
                    self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
                    
                }
            }
        }
    }
    
    
    @IBAction func backBtn(_ sender: Any) {
        dismissDetail()
    }
    
    func save(completion: (_ finished: Bool) -> ()){
        guard let managedContext = appDelegate?.persistentContainer.viewContext else {return}
        let goal = Goal(context: managedContext)
        goal.title = goalDescription
        goal.term = goalType.rawValue
        goal.number = Int32(0)
        goal.progress = Int32(textField.text!)!
        
        do {
          try managedContext.save()
            print("saved succesfully")
            completion(true)
        } catch {
            debugPrint("Could not save\(error.localizedDescription)")
            completion(false)
        }
    }
    
}
