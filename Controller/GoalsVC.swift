//
//  ViewController.swift
//  GOALpost
//
//  Created by Marc Michel on 10/9/18.
//  Copyright © 2018 Marc Michel. All rights reserved.
//

import UIKit
import CoreData


let appDelegate = UIApplication.shared.delegate as? AppDelegate

class GoalsVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var goal: [Goal] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isHidden = false
    }
    
    func fetchCoredata() {
        self.fetch { (complete) in
            if complete {
                if goal.count >= 1 {
                    tableView.isHidden = false
                } else {
                    tableView.isHidden = true
                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchCoredata()
        tableView.reloadData()
    }

    @IBAction func addBtn(_ sender: Any) {
        guard let creatGoalVC = storyboard?.instantiateViewController(withIdentifier: CREATE_VC_ID) else {return}
        presentDetail(creatGoalVC)
        
    }
    
}

extension GoalsVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return goal.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CELL_ID, for: indexPath) as? TableViewCell else { return UITableViewCell()}
        let goals = goal[indexPath.row]
        cell.configureCell(goals: goals)
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .destructive, title: "DELETE") { (rowAction, indexPath) in
            self.removeGoal(atIndexPath: indexPath)
            self.fetchCoredata()
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
        
        let addAction = UITableViewRowAction(style: .normal, title: "ADD 1") { (rowAction, indexPath) in
            self.setProgressForGoal(atIndexPath: indexPath)
            tableView.reloadRows(at: [indexPath], with: .automatic)
        }
        deleteAction.backgroundColor = #colorLiteral(red: 0.8537833122, green: 0.1647766972, blue: 0.1239444788, alpha: 1)
        addAction.backgroundColor = #colorLiteral(red: 0.9771530032, green: 0.7062081099, blue: 0.1748393774, alpha: 1)
        return [deleteAction, addAction]
    }
}

extension GoalsVC {
    func fetch(completion: (_ finished: Bool) ->()) {
        guard let manageContext = appDelegate?.persistentContainer.viewContext else { return }
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Goal")
        do {
            
            goal = try manageContext.fetch(fetchRequest) as! [Goal]
            print("fectching has been succesful")
            completion(true)
        } catch {
            debugPrint("error\(error.localizedDescription)")
            completion(false)
        }
    }
    
    func removeGoal(atIndexPath indexPath: IndexPath) {
        guard let manageContext = appDelegate?.persistentContainer.viewContext else {return}
        manageContext.delete(goal[indexPath.row])
        
        do {
            try manageContext.save()
            print("saved Sucessfully")
        } catch {
            debugPrint("error could not save \(error.localizedDescription)")
        }
    }
    
    func setProgressForGoal(atIndexPath indexPath: IndexPath) {
        guard let managedContext = appDelegate?.persistentContainer.viewContext else {return}
        let chosenGoal = goal[indexPath.row]
        if chosenGoal.number < chosenGoal.progress {
            chosenGoal.number = chosenGoal.number + 1
        } else {
            return
        }
        
        do {
            try managedContext.save()
            print("successfully set Progress")
        } catch {
            debugPrint("error \(error.localizedDescription)")
        }
        
    }
}

