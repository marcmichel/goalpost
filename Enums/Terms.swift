//
//  Terms.swift
//  GOALpost
//
//  Created by Marc Michel on 10/9/18.
//  Copyright © 2018 Marc Michel. All rights reserved.
//

import Foundation

enum Terms: String {
    case shortTerm = "Short Term"
    case longTerm = "Long term"
}
