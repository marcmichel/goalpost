//
//  Button.swift
//  GOALpost
//
//  Created by Marc Michel on 10/11/18.
//  Copyright © 2018 Marc Michel. All rights reserved.
//

import UIKit

extension UIButton {
    func setSelectedcolor() {
        self.backgroundColor = #colorLiteral(red: 0.8537833122, green: 0.1647766972, blue: 0.1239444788, alpha: 1)
    }
    
    func setDeselectedColor() {
        self.backgroundColor = #colorLiteral(red: 1, green: 0.4932718873, blue: 0.4739984274, alpha: 1)
    }


}
