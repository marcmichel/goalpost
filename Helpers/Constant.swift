//
//  Constant.swift
//  GOALpost
//
//  Created by Marc Michel on 10/9/18.
//  Copyright © 2018 Marc Michel. All rights reserved.
//

import Foundation

let CELL_ID = "CELL"

let CREATE_VC_ID = "CreateVC"

let GOAL_VC = "GoalVC"

let CREATE_GOAL_VC = "createGoalVC"
