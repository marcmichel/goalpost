//
//  TableViewCell.swift
//  GOALpost
//
//  Created by Marc Michel on 10/9/18.
//  Copyright © 2018 Marc Michel. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    
    @IBOutlet weak var completionView: UIView!
    @IBOutlet weak var number: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var term: UILabel!
    
    func configureCell(goals: Goal) {
        self.number.text = String(goals.number)
        self.title.text = goals.title
        self.term.text = goals.term
        
        if goals.progress == goals.number {
            self.completionView.isHidden = false
        } else {
            self.completionView.isHidden = true
        }
    }
    

}
